# This file contains a user story for demonstration only.
# Learn how to get started with Behat and BDD on Behat's website:
# http://behat.org/en/latest/quick_start.html



################### ADMIN USERS ##################################

Feature: Comment Feature
    Scenario: Get list of comments for admin users
        Given I set payload
        """
        {
          "email": "admin@admin.com",
          "password": "admin"
        }
        """
        Given I log in
        When I request to "GET" "/comments"
        Then The response status code should be 200
        And The "content-type" header response should exist
        And The "content-type" header response should be "application/ld+json; charset=utf-8"


    Scenario: Get list of comments for admin users
        Given I set payload
        """
        {
          "email": "admin@admin.com",
          "password": "admin"
        }
        """
        Given I log in
        When I request to "GET" "/comments/XXX"
        Then The response status code should be 404


    ################### NORMAL USERS #################################

    
    Scenario: Post of comments for normal users
        Given I set payload
        """
        {
          "email": "$admin@$admin.com",
          "password": "$admin"
        }
        """
        Given I log in
        When I request to "POST" "/comments"
        Then The response status code should be 200
        And The "content-type" header response should exist
        And The "content-type" header response should be "application/ld+json; charset=utf-8"
    

    Scenario: Modification of comments for normal users
        Given I set payload
        """
        {
          "email": "$admin@$admin.com",
          "password": "$admin"
        }
        """
        Given I log in
        When I request to "PUT" "/comments"
        Then The response status code should be 200
        And The "content-type" header response should exist
        And The "content-type" header response should be "application/ld+json; charset=utf-8"

    
    Scenario: Delete of comments for normal users
        Given I set payload
        """
        {
          "email": "$admin@$admin.com",
          "password": "$admin"
        }
        """
        Given I log in
        When I request to "DELETE" "/comments"
        Then The response status code should be 403
        And The "content-type" header response should exist
        And The "content-type" header response should be "application/ld+json; charset=utf-8"

    
    Scenario: Trying to delete a comment without being logged
    When I request to "DELETE" "/comments/{comment2.id}"
    Then The response status code should be 401
    


